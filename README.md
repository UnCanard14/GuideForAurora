<!-- $theme: gaia -->

# Guide pour Aurora

<img src="https://gitlab.com/UnCanard14/GuideForAurora/raw/master/Pictures/1.png" width="" height="200" />

Pour commencer Aurora est un logiciel qui principalement fait le lien entre ce qui se passe dans vos jeux et se qui s’affiche sur vos matériels RGB .

## Summary
* 1 L’interface
* 2 Le commencement 
* 4 L’onglet jeux
	* 4.1 Préférences
		* 4.1.1 General Settings
		* 4.1.2 Volume Overlay
		* 4.1.3 Skype Overlay
		* 4.1.4 Away Effects
		* 4.1.5 Devices & Wrappers
		* 4.1.6 AtmoOrb
		* 4.1.7 Updates
		* 4.1.9 Debug
		* 4.2 Dekstop Settings
		* 4.2.1 Shortcut Assistant
		* 4.2.2 CPU Usage
		* 4.2.3 Ram Usage
		* 4.2.4 Interactive Layer
		* 4.3 – Dota Settings
		* 4.3.1 Dota 2 Respawn
		* 4.3.2 Health Indicator
		* 4.3 .3 Mana Indicator
		* 4.3.4 Dota 2 Ability Keys
		* 4.3.5 Dota 2 Item Keys
		* 4.3.6 Dota 2 Hero Ability Heroes
		* 4.3.7 Dota 2 Killstreaks
		* 4.3.8 Dota 2 Background
	* 4.4  CS:GO Settings
		* 3.2.1 CSGO Typing Indicator
		* 3.2.2 CSGO Kils Indicator
		* 3.2.3 CSGO Flashbang Effect
		* 3.2.4 Health Indicator
		* 3.2.5 Ammo Indicator
		* 3.2.6 CSGO Bomb Effect
		* 3.2.7 CSGO Burning Effect
		* 3.2.8 CSGO Background
	* 4.5 GTA 5 Settings
		* 4.5.1 GTA 5 Police Siren
		* 4.5.2 GTA 5 Background
	* 4.6 Rocket League Settings
		* 4.6.1 Boost Indicator
		* 4.6.2 Boost Indicator (Peripheral)
		* 4.6.3 Rocket League Background
	* 4.7 Overwatch Settings
	* 4.8 Payday 2 Settings
		* 4.8.1 Payday 2 Flashbang
		* 4.8.2 Health Indicator
		* 4.8.3 Ammo Indicator
		* 4.8.4 Payday 2 States
		* 4.8.5 Payday 2 Background
	* 4.9 The Division Settings
	* 4.10 League of Legends Settings
	* 4.11 Hotline Miami Settings
		* 4.11.1 Movement4.11.2 Other Actions
	* 4.12 The Talos Principale Settings
		* 4.12.1 Movement
		* 4.12.2 Other Actions
	* 4.13 Battefield 3 Settings
		* 4.13.1 Movement
		* 4.13.2 Other Actions
	* 4.14 Blacklight : Retribution Settings
		* 4.14.1 Movement	
	* 4.15 Magic : The Gathering – Duels of the
	 	Planes walkers 2012 Settings
	* 4.16 Middle-earth : Shadow of Mordor Settings
		* 4.16.1 Movement4.16.2 Other Actions
	* 4.17 Serious Sam 3 Settings
		* 4.17.1 Movement4.17.2 Other Actions
	* 4.18 Robot Roller-Derby Disco Dodgeball Settings
		* 4.18.1 Movement
	* 4.19 XCOM : Enemy Uknown Settings
		* 4.19.1 Camera Movement
		* 4.19.2 Other Actions
	* 4.20 Evolve Stage 2 Settings
		* 4.20.1 Movement
		* 4.22.2 Skils
	* 4.22 Guild Wars 2 Settings
		* 4.22.1 Movement
		* 4.21.3 Inventory
		* 4.21.2 Weapons
	* 4.21 Metro Last Light Settings
		* 4.21.1 Movement
		* 4.20.2 Other Actions
		* 4.22.3 Targeting
		* 4.22.4 User Interface
		* 4.22.5 Actions
	* 4.23 Worms W.M.D Settings
	* 4.24 Blade and Soul Settings

* 5 Profile
* 6 Animations Editor


## 1 L’interface

<img src="https://gitlab.com/UnCanard14/GuideForAurora/raw/master/Pictures/first.png"/>

Elle se présente sous la forme de trois panneaux différents :

+ Le premier (1) contient les onglets des jeux prit en compte par Aurora. 
+ Le deuxième (2)  contient les possibilité d’éclairage des jeux de l’onglet 1. 
+ Le troisième (3) affiche ce qui s’affiche sur votre clavier.   
+ Le quatrième (4) contient les options d’éclairages de la deuxième fenêtre.

<br/>Vous pouvez cacher les jeux que vous n’utilisez pas en faisant un clique droit et en le cachant, vous pouvez les faire reparaître en cliquant sur : 
    
  <img src="https://gitlab.com/UnCanard14/GuideForAurora/raw/master/Pictures/hide.PNG">
 
## 2 Le commencement 

 
<img src="https://gitlab.com/UnCanard14/GuideForAurora/raw/master/Pictures/begin.PNG">

Tout d’abord vous devez dire à aurora quel matériel vous utilisez pour cela allez dans paramètre et dans l’onglet Devices & Wrappers et référencez votre matériel.

Maintenant vous pouvez commencer à profiter d’aurora avec les paramètres par défaut.

## 4 L’onglet jeux 
  
Vous pouvez configurer l’éclairage de vos jeux selon vos préférences.
  
### 4.1 Préférences
#### 4.1.1 General Settings

Options générales d’Aurora

Quand l'option *"Star with Widows"* est :ballot_box_with_check: Aurora se démarre au démarrage de Windows.

Quand l'option *"Star Aurora in minimized state"* est :ballot_box_with_check: Aurora se démarre en tâche d’arrière plan.

#### 4.1.2 Volume Overlay	
Quand vous changez le volume à l'aide de vos touches médias de votre clavier les touches assignées s'allument en fontion de votre volume Aurora va faire un degradé de couleurs entre les niveaux du volume bas moyen et élevé.

Quand *"Dim the background keys to emphasize the shortcut keys"* :ballot_box_with_check: est activé le fond de votre clavier s'allume de la couleur choisi.

#### 4.1.3 Skype Overlay		
L’éclairage de votre clavier changer en fonction de ce qu’il se passe sur l’application [Skype](https://www.skype.com/fr) , l’overlay vous indique si vous avez un message et si vous recevez des appels.

#### 4.1.4 Away Effects		
 Quand aucun jeux n‘est lancé le clavier s’allume en fonction de l’effet choisi :
* None : aucun effet 
* Dim :
* Color breathing :

<img src="https://gitlab.com/UnCanard14/GuideForAurora/raw/master/Pictures/6.gif" width="" height="200" />

* Rainbow shift (Horizontal):

<img src="https://gitlab.com/UnCanard14/GuideForAurora/raw/master/Pictures/1.gif" width="" height="200" />

* Rainbow shift (Vertical):

<img src="https://gitlab.com/UnCanard14/GuideForAurora/raw/master/Pictures/2.gif" width="" height="200" />

* Star fall:

<img src="https://gitlab.com/UnCanard14/GuideForAurora/raw/master/Pictures/3.gif" width="" height="200" />

* Rain fall:
* Blackout:
* Matrix:

<img src="https://gitlab.com/UnCanard14/GuideForAurora/raw/master/Pictures/5.gif" width="" height="200" />

#### 4.1.5 Devices & Wrappers		
Cet onglet permet de sectionner votre matériel RGB.

*"Preeferred Keyboard Layout"*
Permet de choisir la disposition de votre clavier.

*"Mouse Orientation"*
Permet de choisir l'orientation de votre souris si vous êtes gaucher ou droitier.

*"Keycap type"*
Permet de choisir le style de touche sur l'application.

Vous pouvez désactiver l’éclairage de votre clavier, votre souris ou votre Casque en activant :ballot_box_with_check: l'option.

#### 4.1.6 AtmoOrb
Aurora peut contrôler une [AtmoOrb](https://github.com/ambilight-4-mediaportal/AtmoOrb)

#### 4.1.7 Updates		
Onglet des options des mises à jours.

Le bouton *"Check for Updates"* Permet de vérifier si il n’y a pas de nouvelles mises a jours.

Quand *"Automatically check for updates on program start"* est  :ballot_box_with_check: activé à chaque démarrage Aurora vérifie si il n’existe pas de nouvelles mises à jours.

Quand *"Allow minor updates to be installed in the backgrounds"* :ballot_box_with_check: est activé Les mises à jours mineurs peuvent se faire en arrière plan pendant que aurora est exécuté.

#### 4.1.8 About
Aurora by Antonpup & simon-wh

#### 4.1.9 Debug		
Affiche ce qui doit s’afficher sur votre clavier en bitmap.

### 4.2 Dekstop Settings
#### 4.2.1 Shortcut Assistant
A l’appui d’une des touches raccourcis (Ctrl Win Alt) les touches assignées s’allument.

Quand *"Dim the background keys to emphasize the shortcut keys"* :ballot_box_with_check: est activé le fond de votre clavier s'allume de la couleur choisie.

#### 4.2.2 CPU Usage
Les touches assignées s’allument en fonction de utilisation de votre CPU si il est plus ou moins solicité.

#### 4.2.3 Ram Usage
Les touches assignées s’allument en fonction de utilisation de votre RAM si il est plus ou moins solicité.

#### 4.2.4 Interactive Layer
Quand aucun jeux n ‘est lancé le clavier s’allume en fonction de l’appui de vos touches de clavier.

### 4.3 [Dota](http://dota2.com/) Settings
#### 4.3.1 Dota 2 Respawn
Quand votre hero meurt et revient à la vie les touches assignées changent de couleur.

#### 4.3.2 Health Indicator

Les touches assignées changent de couleur en fonction de la vie de votre héro.

#### 4.3 .3 Mana Indicator

Les touches assignées changent de couleur en fonction de la mana de votre héro.
    
#### 4.3.4 Dota 2 Ability Keys

Les touches qui vous devez assigner en fonction de vos raccourcis dota 2 de sorts changent de couleur en fonction de l’état de vos cpacités.
    
#### 4.3.5 Dota 2 Item Keys

Les touches qui vous devez assigner en fonction de vos raccourcis dota 2 d’objets changent de couleur en fonction de l’état de vos objets.
    
#### 4.3.6 Dota 2 Hero Ability Heroes

Quand vous lancez un sort certains héros ont une animation en fonction du sort lancé.
    
#### 4.3.7 Dota 2 Killstreaks

Dans dota en fonction de votre niveau de killstreak l’arrière plan d’éclairage change de couleur jusqu'àce qui vous mourriez .
    
#### 4.3.8 Dota 2 Background
L’Éclairage d’arrière plan change en fonction de si vous êtes dans l’équipe du Dire ou du Radiant.    
	
### 4.4  [CS:GO](http://blog.counter-strike.net/) Settings
#### 4.2.1 CSGO Typing Indicator

L’orsque vous écrivez un message à votre équipe ou à l’équipe ennemie les touches assignées s’illuminent.
    
#### 4.2.2 CSGO Kils Indicator

Les touches assignées s’allument une par une si vous faites des kills la touche n’a pas la même couleur si vous faites un headshot, quand vous mourez le compteur se remet à zero.

#### 4.2.3 CSGO Flashbang Effect

Quand vous êtes aveuglés par une GSS le clavier s’éclaire entièrement de la couleur choisie.
    
#### 4.2.4 Health Indicator

Les touches assignées s’éclairent en fonction de votre vie.
    
#### 4.2.5 Ammo Indicator

Les touches assignées s’éclairent en fonction de vos munitions.

#### 4.2.6 CSGO Bomb Effect

Les touches assignées s’éclairent en fonction de l’état de la bombe.

#### 4.2.7 CSGO Burning Effect
Quand vous êtes entrain de brûler le clavier s’éclaire entièrement de la couleur choisit vous pouvez désactiver l’animation de brûlure.

#### 4.2.8 CSGO Background
L’arrière plan de votre clavier s’éclaire en fonction de votre équipe (Counter-Terrorists, Terrorists) et ambient quand vous êtes sur le menu du jeu.

### 4.5 [GTA 5](http://www.grandtheftauto5.com/) Settings
#### 4.5.1 GTA 5 Police Siren

Quand vous êtes recherchés par la police les touches assignées se mettent à clignoter.

#### 4.5.2 GTA 5 Background

L'arrière plan change de couleur quand vous changez de personnages ou du type de la course.
	
### 4.6 [Rocket League](https://www.rocketleague.com/) Settings
#### 4.6.1 Boost Indicator

Les touches assignées changent de couleur en fonction du niveau de boost que vous avez.

#### 4.6.2 Boost Indicator (Peripheral)

Les touches assignées changent de couleur en fonction du niveau de boost que vous avez.

#### 4.6.3 Rocket League Background

L'arrière plan de votre clavier change en fonction des buts des équipes.

### 4.7 [Overwatch](https://playoverwatch.com) Settings

L'arrière de votre clavier s'allume en fonction de la couleur de votre personnage.

### 4.8 [Payday 2](http://crimenet.info) Settings
#### 4.8.1 Payday 2 Flashbang

#### 4.8.2 Health Indicator

Les touches assignées s’éclairent en fonction de votre vie.

#### 4.8.3 Ammo Indicator

Les touches assignées s’éclairent en fonction de vos munitions.

#### 4.8.4 Payday 2 States

#### 4.8.5 Payday 2 Background
	
### 4.9 [The Division](https://tomclancy-thedivision.ubisoft.com) Settings
	
### 4.10 [League of Legends](euw.leagueoflegends.com) Settings

### 4.11 [Hotline Miami](http://store.steampowered.com/app/219150/Hotline_Miami/) Settings
#### 4.11.1 Movement

#### 4.11.2 Other Actions

### 4.12 [The Talos Principale](http://www.croteam.com/talosprinciple/) Settings
#### 4.12.1 Movement

#### 4.12.2 Other Actions

### 4.13 [Battefield 3](https://www.battlefield.com/en-gb/games/battlefield-3?setLocale=en-gb) Settings
#### 4.13.1 Movement

#### 4.13.2 Other Actions

### 4.14 [Blacklight : Retribution](http://www.arcgames.com/en/games/blacklight-retribution) Settings
#### 4.14.1 Movement

### 4.15 [Magic : The Gathering – Duels of the Planes walkers 2012](http://store.steampowered.com/app/49470/Magic_The_Gathering__Duels_of_the_Planeswalkers_2012/) Settings	

### 4.16 [Middle-earth : Shadow of Mordor](http://store.steampowered.com/app/241930/Middleearth_Shadow_of_Mordor/) Settings
#### 4.16.1 Movement

#### 4.16.2 Other Actions

### 4.17 [Serious Sam 3](http://www.croteam.com/serioussam3bfe/) Settings
#### 4.17.1 Movement

#### 4.17.2 Other Actions

### 4.18 [Robot Roller-Derby Disco Dodgeball](http://store.steampowered.com/app/270450/) Settings
#### 4.18.1 Movement

### 4.19 [XCOM : Enemy Uknown Settings](http://store.steampowered.com/app/200510/XCOM_Enemy_Unknown/)
#### 4.19.1 Camera Movement

#### 4.19.2 Other Actions

### 4.20 [Evolve Stage 2](https://evolvegame.com/) Settings
#### 4.20.1 Movement

#### 4.20.2 Other Actions

### 4.21 [Metro Last Light](http://store.steampowered.com/app/287390/Metro_Last_Light_Redux/) Settings
#### 4.21.1 Movement

#### 4.21.2 Weapons

#### 4.21.3 Inventory

### 4.22 [Guild Wars 2](https://www.guildwars2.com/) Settings
#### 4.22.1 Movement

#### 4.22.2 Skils

#### 4.22.3 Targeting

#### 4.22.4 User Interface

#### 4.22.5 Actions
	
### 4.23 [Worms W.M.D](http://store.steampowered.com/app/327030/Worms_WMD/) Settings

### 4.24 [Blade and Soul](http://www.bladeandsoul.com) Settings

## 5 Profile
Vous pouvez télècharger des profils sur [Mod Workshop](https://modworkshop.net/mydownloads.php?action=browse_cat&cid=299)

## 6 Animations Editor
Quam ob rem circumspecta cautela observatum est deinceps et cum edita montium petere coeperint grassatores, loci iniquitati milites cedunt. ubi autem in planitie potuerint reperiri, quod contingit adsidue, nec exsertare lacertos nec crispare permissi tela, quae vehunt bina vel terna, pecudum ritu inertium 

## 7 Show Profile Overview/Option
### 7.1 Overwiew
Vous pouvez désactiver l’éclairage de votre jeux en cliquant sur :ballot_box_with_check:  de l’option *"Enable Aurora to provide effects with "Nom de votre jeux""*

### 7.2 Preview
Simulation de vos paramètres d’éclairage. Sans avoir besoin de lancer le jeux concerné.

<img src="https://gitlab.com/UnCanard14/GuideForAurora/raw/master/Pictures/Preview.PNG" width="" height="150" />

## 8 Layers

Vous pouvez ajouter des couches d'éclairage.

## 9 Add a new lighting Profile

Vous pouvez ajouter des couches d'éclairage sur des applications qui ne sont pas sur aurora.

## 10 Retrievable Option
### 10.1 Affected Keys
<img src="https://gitlab.com/UnCanard14/GuideForAurora/raw/master/Pictures/keys.PNG" width="" height="210" />

Vous pouvez assigner des touches en cliquant sur *"Assign Keys"* et sur les touches que vous voulez assigner sur shema du clavier ou en appuyant sur les touches de votre clavier. 
Vous pouvez supprimer les touches assignées en les selctionnant et en appyant sur bouton *"Remove"*.
Vous pouvez mettre dans l'ordre les touches en les selctionnant et en appuyant sur *"Move Up"* ou sur *"Move Down"* la premiere sera en haut de la liste elles seront rangées de droite à gauche pour les barres de statuts.

<img src="https://gitlab.com/UnCanard14/GuideForAurora/raw/master/Pictures/Freestyle.PNG" width="" height="150" />

Quand l'option *"Use Freestyle instead"* est activée :ballot_box_with_check: elle crée une zone qui est adptable vous placez la zone sur les touches que vous voullez assigner elles seront triées de gauche à droite.

### 10.2 Reverse blink tthreshold
Quand l'option *"Reverse blink threshold"* est activé :ballot_box_with_check: elle permet de faire clignoter les leds assignés des barres de statuts, quand le niveau est en dessous d'un certain seuil que vous pouvez déterminer à partit d'un pourcentage.

### 10.3 Assigner couleur
Il est possible d'échanger entre deux modes le mode "Stantard" et le mode "Avancé"

Mode standard:
Vous pouvez choisir une couleur sur les couleurs prédéfinies en cliquant dessus

Mode avancé:
Vous pouvez choisir une couleur en cliquant sur la palette ou en ajustant le rouge "R" et le vert "G" et le bleu "B" avec les sélecteurs le "A" permet de changer la transparence


















